# -*- coding: utf-8 -*-

""" iopy/version.py """
__version__ = "0.2.3"

# 0.2.3  (16/10/2020): Correct bug in Fx2Core() connect() method.
# 0.2.2  (02/07/2020): Add selection of Fx2 device through usb bus and
#                      usb address in addition to vid and pid parameters.
# 0.2.1  (14/02/2020): Correct connect() method of fx2.Fx2Core class.
# 0.2.0  (27/09/2019): Update dependance of the library pyusb
#                      from v1.0.0a3 to v1.0.2.
# 0.1.2  (10/07/2018): Add raw_read method to PrologixGpibEth class.
# 0.1.1  (10/04/2018): Update Fx2Core class.
# 0.1.0  (08/12/2016): First version.
